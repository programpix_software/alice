﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Alice
{
    class Program
    {
        void Cliente()
        {
            object body = null;
           
                // Create new operation data
                Operation operation = new Operation();
                operation.OperatorId = Constant.DEMO_OPERATOR_ID;
                operation.TerminalId = Constant.DEMO_TERMINAL_ID;
                operation.Observations = string.Format("{0} {1} operation", Constant.DEMO_TERMINAL_ID, this.SelectedOption.ToString());
                operation.Amount = Convert.ToInt32(this.numericUpDownAmount.Value);
                operation.Destination = (OperationDestination)this.comboBoxDestination.SelectedIndex;
                if (this.SelectedOption != Options.SALE)
                    operation.Denominations = Manager.Denomination.GetDenominationReqsFromDataSource(this.dataGridViewDenominations.DataSource);

                if (this.SelectedOption == Options.CHANGE)
                {
                    bool checkDenominations = this.checkBoxCheckDenominations.Checked;
                    bool singleDenomination = this.checkBoxSingleDenomination.Checked;
                    body = new RequestBody2(operation, checkDenominations, singleDenomination);
                }
                else
                {
                    // Create body with operation
                    body = new RequestBody(operation);
                }
           
            Manager.Http.SendRequestAsync(HttpMethodType.POST, this.ActionUri, body);


        }


        static async System.Threading.Tasks.Task Main(string[] args)
        {
            Operation operation = new Operation();
            operation.Amount = 500;
            operation.OperatorId = "operator_id";
            operation.TerminalId = "1";
            operation.Observations = "assd";

            Operation operation1 = new Operation(operation);
            //var json = JsonConvert.SerializeObject(operation1);
            string json = "{ \"operation\": { \"amount\": 650, \"operator_id\": \"1\"," +
        "\"terminal_id\": \"1\",\"observations\": \"string\",\"reference\": \"string\"}}";

            JObject o = JObject.Parse(json);

            string data1 = json.ToString();
            var data = new StringContent(json, Encoding.UTF8, "application/json");
            string v = data.ToString();

            var url = "https://127.0.0.1:8081/api/operation/sale";
             var client = new HttpClient();
            var byteArray = Encoding.ASCII.GetBytes("admin:admin");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

            ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;
            var p = await client.PostAsync(url, data);
          
            if( p.IsSuccessStatusCode)
            {
                HttpContent content = p.Content;
                string v1 = await content.ReadAsStringAsync();
                JObject jObjects= JObject.Parse(v1);
                JToken jTokens = jObjects["data"]["operation"]["operation_id"];
                string v2 = jTokens.ToString();
                HttpResponseMessage httpResponseMessage = await client.GetAsync("https://127.0.0.1:8081/api/operation?" +
                    "operation_id=" + v2);
                new DateTime();
                

            }

            Console.WriteLine("Press Enter to exit");
            Console.ReadLine();

            //var response = await client.PostAsync(url, data);




        }
    }
}
