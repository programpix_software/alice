﻿
using Newtonsoft.Json;
using System;
using System.Collections.Generic;



    public class Operation
    {
        #region Properties

        [JsonProperty(PropertyName = "operation_id")]
        public int OperationId { get; set; }

       

        [JsonProperty(PropertyName = "amount")]
        public int Amount { get; set; }

        [JsonProperty(PropertyName = "total_in")]
        public int TotalIn { get; set; }

        [JsonProperty(PropertyName = "total_out")]
        public int TotalOut { get; set; }


        [JsonProperty(PropertyName = "date")]
        public DateTime? Date { get; set; }

        [JsonProperty(PropertyName = "operator_id")]
        public string OperatorId { get; set; }

        [JsonProperty(PropertyName = "terminal_id")]
        public string TerminalId { get; set; }

        [JsonProperty(PropertyName = "observations")]
        public string Observations { get; set; }

      

        #endregion Properties

        #region Constructors

        public Operation()
        {
        }

        public Operation(Operation operation)
        {
            if (operation != null)
            {
                this.Amount = operation.Amount;
                this.Date = operation.Date;
              
                this.Observations = operation.Observations;
                this.OperationId = operation.OperationId;
                this.OperatorId = operation.OperatorId;
              
                this.TerminalId = operation.TerminalId;
                this.TotalIn = operation.TotalIn;
                this.TotalOut = operation.TotalOut;
              

            
            }
        }

        #endregion Constructors
    }
